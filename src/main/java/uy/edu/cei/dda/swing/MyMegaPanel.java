package uy.edu.cei.dda.swing;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SpringLayout;
import java.awt.Color;

public class MyMegaPanel extends JPanel {

	/**
	 * Create the panel.
	 */
	public MyMegaPanel() {
		setBackground(Color.ORANGE);
		SpringLayout springLayout = new SpringLayout();
		setLayout(springLayout);
		
		JLabel lblNewLabel = new JLabel("MegaUltraPanel");
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel, 10, SpringLayout.NORTH, this);
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel, 29, SpringLayout.WEST, this);
		add(lblNewLabel);

	}

}
