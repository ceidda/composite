package uy.edu.cei.dda.swing;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.SpringLayout;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MyApplicationWindow {

	private JFrame frame;
	private int north = 10;
	private int west = 60;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MyApplicationWindow window = new MyApplicationWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MyApplicationWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JPanel panel_1 = new JPanel();
		panel_1.setBounds(78, 41, 317, 163);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(null);
		
		JButton btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(78, 0, 93, 29);
		
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MyMegaPanel newPanel = new MyMegaPanel();
				newPanel.setBounds(north, west, 147, 56);

				panel_1.add(newPanel);
				
				north += 10;
				west += 10;
				
				panel_1.revalidate();
				panel_1.repaint();
			}
		});
		frame.getContentPane().add(btnAgregar);
		
		
		JPanel panel = new MyMegaPanel();
		panel.setBounds(158, 5, 0, 0);
		panel_1.add(panel);
	}
}
