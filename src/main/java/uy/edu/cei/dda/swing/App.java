package uy.edu.cei.dda.swing;

/**
 * Hello world!
 *
 */
public class App {
	
	static class MyComponent extends Composite {
		public MyComponent(final String name) {
			super(name);
			
		}
	}

	static class MyLeaf extends Leaf {
		public MyLeaf(final String name) {
			super(name);
		}
	}

	public static void main(String[] args) {
		Composite c1 = new MyComponent("c1");
		Composite c2 = new MyComponent("c2");
		c1.addComponent(c2);
		c1.addComponent(new MyLeaf("l2"));
		c1.addComponent(new MyLeaf("l3"));
		c2.addComponent(new MyLeaf("l1"));
		c1.print();
	}
}
