package uy.edu.cei.dda.swing;

public abstract class Leaf implements Component {

	private final String name;

	public Leaf(final String name) {
		this.name = name;
	}

	@Override
	public void print() {
		System.out.println(String.format("%s - %s", this.getClass(), this.name));
	}

}
