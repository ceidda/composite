package uy.edu.cei.dda.swing;

import java.util.LinkedList;
import java.util.List;

public abstract class Composite implements Component {

	private final List<Component> components;
	private final String name;
	
	public Composite(final String name) {
		this.components = new LinkedList<>();
		this.name = name;
	}
	
	public void addComponent(final Component component) {
		this.components.add(component);
	}
	
	@Override
	public void print() {
		System.out.println(String.format("%s - %s", this.getClass(), this.name));
		components.forEach(Component::print);
	}
	
}
